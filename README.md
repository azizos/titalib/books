# Books

## BOOKS API

As a part of the TitaLib application, our Books API, written in Go, is used to view or modify books (that can be borrowed by Titanic survivors).

### What does this service do?

_Books API_ is a [RESTful](https://en.wikipedia.org/wiki/Representational_state_transfer) API helping you to view or update books.
The service is written in Go and offers several HTTP endpoints (described below) using [Gin](https://github.com/gin-gonic/gin) framework. To persist data, we're using a Mongo DB database.

In this repository, we're using [_books-collector_](https://gitlab.com/azizos/titalib/books-collector) project as a git _submodule_. In order to run the application locally, we'll have to work with a pre-populated database. Therefore, using `docker-compose`, we build and run an image of `books-collector` (using its `Dockerfile` in the submodule folder). 
Once the `docker-compose up` command is executed (_see below_), initially a Mongo database will run (in a container) and then the books-collector container will run and store book objects. After that, we can simply use our books API to interact with that already populated database.

Below a sample Book object:

```json
{
    "_id": "5ea1d16ccc5a767b4f468d08",
    "primary_isbn10": "0525539522",
    "title": "MASKED PREY",
    "author": "John Sandford",
    "added_on": "2020-04-23",
    "pages_count": 400,
    "words_count": 95700,
    "avg_reading_time_per_hour": 6.38,
    "type": "hardcover-fiction"
}
```

#### Available endpoints

| Method | Path               | Description                               |
| ------ | ------------------ | ----------------------------------------  |
| GET    | /books             | To retrieve a list of books               |
| GET    | /books/isbn/[isbn] | To retrieve a book by its _isbn_ property |
| GET    | /books/[id]        | To retrieve a book by its __id_ property  |
| PUT    | /books/[id]        | To update a book by its _id property      |
| DELETE | /books/[id]        | To delete a book by its _id property      |

#### Postman collection

*todo*

## Run
To run this service locally, you can choose to use `minikube` (k8s manifest in the `env` repository) or you could use `docker-compose` (recommended). 
In case you choose for `docker-compose`, please make sure the following environment variable already available on your machine:

| Env         | Description                                                                                                                                             |
| ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- |
| NYT_API_KEY | An API-key needed by `books-collector` to get New York Times best-seller books. _See here: https://developer.nytimes.com/docs/books-product/1/overview_ |

Run the command below in this project's root directory and the API is locally available on port 8080 (by default) :)
-`docker-compose up --build --remove-orphans`