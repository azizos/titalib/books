package common

import "github.com/op/go-logging"

// GetLogModule is used to create and return a formatted logging module to be used globally in app
func GetLogModule() (*logging.Logger, error) {
	var format = logging.MustStringFormatter(
		`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.6s} %{id:03x}%{color:reset} %{message}`,
	)
	logging.SetFormatter(format)
	return logging.GetLogger("app")
}
