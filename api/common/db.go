package common

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var log, _ = GetLogModule()

// ConnectDB makes connection with the database
func ConnectDB(mongoURI string, mongoDB string) (*mongo.Database, context.Context) {
	log.Info("Connecting to the database..")
	ctx, _ := context.WithTimeout(context.Background(), 120*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(mongoURI))
	if err != nil {
		log.Panicf("Error connecting to database: %v", err)
	}
	database := client.Database(mongoDB)
	return database, ctx

}
