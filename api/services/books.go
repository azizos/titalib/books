package services

import (
	"context"
	"time"

	"gitlab.com/azizos/titalib/books/api/common"
	"gitlab.com/azizos/titalib/books/api/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2/bson"
)

var log, _ = common.GetLogModule()
var ctx context.Context
var ctxCancelFunc context.CancelFunc
var timeTillContextDeadline = 2 * time.Second

// ListBooks retrieves all Book objects from the database
func ListBooks(collection *mongo.Collection) []*models.Book {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	var books []*models.Book
	cursor, err := collection.Find(ctx, bson.M{})
	defer cursor.Close(ctx)
	if err != nil {
		log.Debug("No books returned from the database:", err)
	} else {
		for cursor.Next(context.TODO()) {
			var result *models.Book
			err := cursor.Decode(&result)
			if err != nil {
				log.Debug("Problem retrieving book object:", err)
			} else {
				books = append(books, result)
			}
		}
	}
	return books
}

// GetBook finds one single book object based on id
func GetBook(collection *mongo.Collection, id string) *models.Book {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	var result *models.Book
	objectID, _ := primitive.ObjectIDFromHex(id)
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&result)
	if err != nil {
		log.Errorf("Problem finding a document: %v", err)

	}
	return result
}

// GetBookByISBN finds one single book object based on isbn
func GetBookByISBN(collection *mongo.Collection, isbn string) *models.Book {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	var result *models.Book
	err := collection.FindOne(ctx, bson.M{"primary_isbn10": isbn}).Decode(&result)
	if err != nil {
		log.Errorf("Problem finding a document: %v", err)

	}
	return result
}

// UpdateBook finds one single book object based on id
func UpdateBook(collection *mongo.Collection, id string, newBookObject *models.Book) *models.Book {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	objectID, _ := primitive.ObjectIDFromHex(id)
	var result *models.Book
	filterQuery := bson.M{"_id": objectID}
	res := collection.FindOneAndReplace(ctx, filterQuery, newBookObject)
	if res.Err() != nil {
		log.Errorf("Problem updating a document: %v: %v", id, res.Err)

	}
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&result)
	if err != nil {
		log.Errorf("Problem finding a document: %v", err)
	}
	return result
}

// DeleteBook delete one single book object based on id
func DeleteBook(collection *mongo.Collection, id string) error {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	objectID, _ := primitive.ObjectIDFromHex(id)

	res := collection.FindOneAndDelete(ctx, bson.M{"_id": objectID})
	if res.Err() != nil {
		log.Errorf("Problem removing a document: %v: %v", objectID, res.Err)
	}
	return res.Err()
}
