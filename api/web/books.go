package api

import (
	"gitlab.com/azizos/titalib/books/api/common"
	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/azizos/titalib/books/api/models"
	"gitlab.com/azizos/titalib/books/api/services"

	"github.com/gin-gonic/gin"
)

var log, _ = common.GetLogModule()

// List is used to retrieve a list of books objects
func List(context *gin.Context) {
	db := context.MustGet("db").(*mongo.Collection)
	books := services.ListBooks(db)
	if len(books) <= 0 {
		context.AbortWithStatus(204)
		return
	}
	context.JSON(200, books)
	return
}

// Get is used to retrieve a book object by its id OR isbn property: responding to /:id or /isbn/:isbn
func Get(context *gin.Context) {
	db := context.MustGet("db").(*mongo.Collection)
	param1 := context.Param("param1")
	isbn := context.Param("isbn")
	var book *models.Book
	if param1 == "isbn" && isbn != "" {
		book = services.GetBookByISBN(db, isbn)
		if book == nil {
			context.AbortWithStatus(204)
			return
		}
	} else {
		book = services.GetBook(db, param1)
	}
	if book == nil {
		context.AbortWithStatus(204)
		return
	}
	context.JSON(200, book)
	return
}

// Update is used to update a book object by its _id property
func Update(context *gin.Context) {
	var book *models.Book
	db := context.MustGet("db").(*mongo.Collection)
	bookID := context.Param("id")
	if err := context.ShouldBindJSON(&book); err != nil {
		context.AbortWithStatus(400)
		return
	}
	changedBook := services.UpdateBook(db, bookID, book)
	if changedBook == nil {
		context.AbortWithStatus(404)
		return
	}
	context.JSON(200, changedBook)
	return
}

// Delete is used to update a book object by its _id property
func Delete(context *gin.Context) {
	db := context.MustGet("db").(*mongo.Collection)
	bookID := context.Param("id")
	err := services.DeleteBook(db, bookID)
	if err != nil {
		context.AbortWithStatus(400)
		return
	}
	context.Status(200)
	return
}
