package models

// Book struct is used to store and parse book objects
type Book struct {
	ID                    string  `json:"_id,omitempty" bson:"_id,omitempty" `
	ISBN                  string  `json:"primary_isbn10" bson:"primary_isbn10"`
	Title                 string  `json:"title" bson:"title"`
	Author                string  `json:"author" bson:"author"`
	CreationDate          string  `json:"added_on" bson:"added_on"`
	PagesCount            int32   `json:"pages_count" bson:"pages_count"`
	WordsCount            int32   `json:"words_count" bson:"words_count"`
	AvgReadingTimePerHour float64 `json:"avg_reading_time_per_hour" bson:"avg_reading_time_per_hour"`
	BookType              string  `json:"type" json:"type"`
}
